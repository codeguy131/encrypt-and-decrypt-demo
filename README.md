# Encryption and decryption

A very basic example of encrypting a message using AES 256 CBC.
The AES key is then encrypted using a public key.
The message is decrypted using the AES key which is decrypted by the private key
corresponding to the public key used for encryption.

### Depends on [rust-openssl](https://github.com/sfackler/rust-openssl)

## Usage

Pass a message as an arg when running

`cargo run -- your-message-here`
