use std::env::args;

use openssl::{
    base64,
    encrypt::{Decrypter, Encrypter},
    pkey::PKey,
    rand::rand_bytes,
    rsa::{Padding, Rsa},
    symm::{decrypt, Cipher, Crypter, Mode},
};
type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn encrypt(message: String) -> Result<()> {
    println!("Hello, world!");

    // Generate keys
    let rsa = Rsa::generate(4096)?;
    let key = PKey::from_rsa(rsa)?;

    // Get public key from PKEY
    let pub_key = PKey::public_key_from_der(&key.public_key_to_der()?)?;

    let msg = message.as_bytes();

    // Holds encrypted message
    let mut enc_buff = vec![0u8; msg.len() + Cipher::aes_256_cbc().block_size()];

    // Random generated AES key
    let mut akey = vec![0u8; 32];
    let mut iv = vec![0u8; 16];
    rand_bytes(&mut akey)?;
    rand_bytes(&mut iv)?;

    // Print AES key to screen
    let aes_key_read = base64::encode_block(&akey);
    println!("AES key: {}", aes_key_read);

    let msg_str = String::from_utf8(msg.to_vec())?;

    println!("Original message: {}", msg_str);

    // For encrypting using AES key
    let mut crp = Crypter::new(Cipher::aes_256_cbc(), Mode::Encrypt, &akey, Some(&iv))?;

    // For decrypting using AES key
    let mut dec_crp = Crypter::new(Cipher::aes_256_cbc(), Mode::Decrypt, &akey, Some(&iv))?;

    // Encrypt message with AES 256 CBC
    let mut enc_len = crp.update(msg, &mut enc_buff)?;
    enc_len += crp.finalize(&mut enc_buff[enc_len..])?;
    enc_buff.truncate(enc_len);
    let readable = base64::encode_block(&enc_buff);

    println!("AES Encrypted message: {}", readable);

    // Decrypt message with AES key
    let mut dec_buff = vec![0u8; enc_len + Cipher::aes_256_cbc().block_size()];
    let mut dec_len = dec_crp.update(&enc_buff, &mut dec_buff)?;
    dec_len += dec_crp.finalize(&mut dec_buff[dec_len..])?;
    dec_buff.truncate(dec_len);

    let dec_read = String::from_utf8(dec_buff.to_owned())?;
    println!("AES decrypted message: {}", dec_read);

    // For encrypting using public key
    let mut encrypter = Encrypter::new(&pub_key)?;
    encrypter.set_rsa_padding(Padding::PKCS1_OAEP)?;

    // Encrypt AES key using public key
    let enc_len = encrypter.encrypt_len(&akey)?;
    let mut a_buff = vec![0u8; enc_len];
    let fin_len = encrypter.encrypt(&akey, &mut a_buff)?;
    a_buff.truncate(fin_len);

    let a_read = base64::encode_block(&a_buff);
    println!("Encrypted AES key: {}", a_read);

    // Decrypt AES key using private PKey
    let mut decrypter = Decrypter::new(&key)?;
    decrypter.set_rsa_padding(Padding::PKCS1_OAEP)?;

    let adec_len = decrypter.decrypt_len(&a_buff)?;
    let mut adec = vec![0u8; adec_len];
    let afin_len = decrypter.decrypt(&a_buff, &mut adec)?;
    adec.truncate(afin_len);

    let adec_read = base64::encode_block(&adec);

    println!("Decrypted AES key: {}", adec_read);

    // Decrypt message using decrypted AES key
    let a_dec_fin = decrypt(Cipher::aes_256_cbc(), &adec, Some(&iv), &enc_buff)?;
    let afin_read = String::from_utf8(a_dec_fin)?;
    println!("Decrypted message: {}", afin_read);

    Ok(())
}

fn main() {
    match args().nth(1) {
        Some(arg) => encrypt(arg).unwrap(),
        None => panic!("Need message to encrypt. Enter as argument"),
    }
}
